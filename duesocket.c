/*OCCHIO AI PUNTATORI */


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>


#define SOCKET_ERROR   ((int)-1)
#define MASSIMO 1000000 /* 1000000 */
#define SIZEBUF MASSIMO
#define MAXSIZE MASSIMO 
#define N_THREADS 5
#define SYNC_MAX N_THREADS+1

pthread_mutex_t sync_lock_s;
pthread_mutex_t sync_lock_c;
pthread_cond_t sync_cond_s;
pthread_cond_t sync_cond_c;
int sync_count_s=0;
int sync_count_c=0;

void usage(void)
{  printf ("usage: servTCP LOCAL_PORT_NUMBER\n"); exit(1); }


void SyncPoint_s(pthread_mutex_t sync_lock_t, pthread_cond_t sync_cond_t) 
{ 
   /* blocca l'accesso al counter */ 
   pthread_mutex_lock(&sync_lock_s); 
  
  /* incrementa il counter di quelli arrivati*/ 
   sync_count_s++; 

   /* controlla se deve aspettare o no */ 
   if (sync_count_s < SYNC_MAX) {
         /* aspetta */
         printf("Aspetto! counter a %d\n",sync_count_s); 
         pthread_cond_wait(&sync_cond_s, &sync_lock_s); 
	 }
   else    {
         /* tutti hanno raggiunto il punto di barriera */ 
         printf("Ci Siamo TUTTI!\n");
         pthread_cond_broadcast (&sync_cond_s); 	 
   }

   /* sblocca il mutex */ 
   printf("Sblocco Mutex! server\n");
   pthread_mutex_unlock (&sync_lock_s);  /* senza l'unlock ne termina solo 1 */ 
   return; 
} 

void SyncPoint_c(pthread_mutex_t sync_lock_t, pthread_cond_t sync_cond_t) 
{ 
   /* blocca l'accesso al counter */ 
   pthread_mutex_lock(&sync_lock_c); 
  
  /* incrementa il counter di quelli arrivati*/ 
   sync_count_c++; 

   /* controlla se deve aspettare o no */ 
   if (sync_count_c < SYNC_MAX) {
         /* aspetta */
         printf("Aspetto! counter a %d\n",sync_count_c); 
         pthread_cond_wait(&sync_cond_c, &sync_lock_c); 
	 }
   else    {
         /* tutti hanno raggiunto il punto di barriera */ 
         printf("Ci Siamo TUTTI!\n");
         pthread_cond_broadcast (&sync_cond_c); 	 
   }

   /* sblocca il mutex */ 
   printf("Sblocco Mutex! client \n");
   pthread_mutex_unlock (&sync_lock_c);  /* senza l'unlock ne termina solo 1 */ 
   return; 
} 
void *Mezzo(void *param){
	struct sockaddr_in Local, Cli;
	short int socketfd, local_port_number;
	int newsocketfd1, OptVal, ris;
	unsigned int len;
	int n, nread, nwrite;
	char buf[MAXSIZE];
	local_port_number = *((short int*) param);
	free(param);
	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketfd == SOCKET_ERROR) {
		printf ("socket() failed, Err: %d \"%s\"\n", errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	OptVal = 1;
	/*printf ("setsockopt()\n");*/
	ris = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
	if (ris == SOCKET_ERROR)  {
		printf ("setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n", errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	memset ( &Local, 0, sizeof(Local) );
	Local.sin_family		=	AF_INET;
	/* indicando INADDR_ANY viene collegato il socket all'indirizzo locale IP     */
	/* dell'interaccia di rete che verrà utilizzata per inoltrare il datagram IP  */
	Local.sin_addr.s_addr	=	htonl(INADDR_ANY);         /* wildcard */
	Local.sin_port		=	htons(local_port_number);
	/*printf ("bind()\n");*/
	ris = bind(socketfd, (struct sockaddr*) &Local, sizeof(Local));
	if (ris == SOCKET_ERROR)  {
		printf ("bind() failed, Err: %d \"%s\"\n",errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	/*printf ("listen()\n");*/
	ris = listen(socketfd, 10 );
	if (ris == SOCKET_ERROR)  {
		printf ("listen() failed, Err: %d \"%s\"\n",errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	SyncPoint_s(sync_lock_s,sync_cond_s);
	do {
  	memset ( &Cli, 0, sizeof(Cli) );
  	len=sizeof(Cli);
  	/*printf ("accept()\n");*/
  	newsocketfd1 = accept(socketfd, (struct sockaddr*) &Cli, &len);
	} while( (newsocketfd1<0)&&(errno==EINTR) );
	if (newsocketfd1 == SOCKET_ERROR)  {
		printf ("accept() failed, Err: %d \"%s\"\n",errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	  printf("connection from %s : %d\n", 
  	inet_ntoa(Cli.sin_addr), 
  	ntohs(Cli.sin_port)
  );

  /* wait for data */
  nread=0;
  /*printf ("read()\n");*/
  while( (n=read(newsocketfd1, &(buf[nread]), MAXSIZE )) >0) {
	{
		printf("letti %d bytes    tot=%d\n", n, n+nread);
		fflush(stdout);
	}
     nread+=n;
     if(buf[nread-1]=='\0')
        break; /* fine stringa */
  }
  if(n<=0) {
    char msgerror[1024];
    sprintf(msgerror,"read() failed [err %d] ",errno);
    perror(msgerror); 

    n=close(newsocketfd1);
    /*return(1);*/
  }
  /* traslazione */
  for( n=0; n<nread  -1  ; n++)
     buf[n] = buf[n]+2;

  /* scrittura */
  nwrite=0;
  /*printf ("write()\n");*/
  while( (n=write(newsocketfd1, &(buf[nwrite]), nread-nwrite)) >0 )
     nwrite+=n;
  if(n<0) {
    char msgerror[1024];
    sprintf(msgerror,"write() failed [err %d] ",errno);
    perror(msgerror); /*return(1);*/
  }

  /* chiusura */
  /*printf ("close()\n");*/
  close(newsocketfd1);
  close(socketfd);
  pthread_exit(NULL);
}

void *STA(void *param){
	struct sockaddr_in Local, Serv;
	char string_remote_ip_address[100]="127.0.0.1";
	short int remote_port_number, local_port_number;
	int socketfd, OptVal, msglen, Fromlen, ris;
	int n, i, nread, nwrite, len;
	char buf[MAXSIZE];
	char msg[MAXSIZE]="012345ABCDfdsfdhgfshfdjyfjytgjSgdnsfhjsfjsjgSFGERAGSDGSDFGSDGTREGgfdsgsdhsdfhsfjsfjshdhsdghfgdhgfdhdfghdfghdgfhdfghfhfgdhdfgh";
	
	remote_port_number = *((short int*) param);
	free(param);
	
	/* get a datagram socket */
	printf ("socket()\n");
	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketfd == SOCKET_ERROR) {
		printf ("socket() failed, Err: %d \"%s\"\n", errno,strerror(errno));
		/*exit(1);*/
	}

	/* avoid EADDRINUSE error on bind() */
	OptVal = 1;
	printf ("setsockopt()\n");
	ris = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
	if (ris == SOCKET_ERROR)  {
		printf ("setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n", errno,strerror(errno));
		/*exit(1);*/
	}

	/* name the socket */
	memset ( &Local, 0, sizeof(Local) );
	Local.sin_family		=	AF_INET;
	/* indicando INADDR_ANY viene collegato il socket all'indirizzo locale IP     */
	/* dell'interaccia di rete che verrà utilizzata per inoltrare i dati          */
	Local.sin_addr.s_addr	=	htonl(INADDR_ANY);         /* wildcard */
	Local.sin_port	=	htons(0);
	printf ("bind()\n");
	ris = bind(socketfd, (struct sockaddr*) &Local, sizeof(Local));
	if (ris == SOCKET_ERROR)  {
		printf ("bind() failed, Err: %d \"%s\"\n",errno,strerror(errno));
		exit(1);
	}

	/* assign our destination address */
	memset ( &Serv, 0, sizeof(Serv) );
	Serv.sin_family	 =	AF_INET;
	Serv.sin_addr.s_addr  =	inet_addr(string_remote_ip_address);
	Serv.sin_port		 =	htons(remote_port_number);
	SyncPoint_c(sync_lock_c,sync_cond_c);
	/* connection request */
	printf ("connect()\n");
	ris = connect(socketfd, (struct sockaddr*) &Serv, sizeof(Serv));
	if (ris == SOCKET_ERROR)  {
		printf ("connect() failed, Err: %d \"%s\"\n",errno,strerror(errno));
		exit(1);
	}
	printf ("dopo connect()\n");
	fflush(stdout);


	/* scrittura */
	len = strlen(msg)+1;
	nwrite=0;
	/*printf ("write()\n");*/
	fflush(stdout);
	while( (n=write(socketfd, &(msg[nwrite]), len-nwrite)) >0 )
		nwrite+=n;
	if(n<0) {
		char msgerror[1024];
		sprintf(msgerror,"write() failed [err %d] ",errno);
		perror(msgerror);
		fflush(stdout);
		/*return(1);*/
	}

	/* lettura */
	nread=0;
	/*printf ("read()\n");*/
	fflush(stdout);
	while( (len>nread) && ((n=read(socketfd, &(buf[nread]), len-nread )) >0))
	{
		nread+=n;
		printf("read effettuata, risultato n=%d len=%d nread=%d len-nread=%d \n", n, len, nread, len-nread );
		fflush(stdout);
	}
	if(n<0) {
		char msgerror[1024];
		sprintf(msgerror,"read() failed [err %d] ",errno);
		perror(msgerror);
		fflush(stdout);
		/*return(1);*/
	}


  /* chiusura */
  close(socketfd);
	
}

void *casino(){
	printf("AHAHAH \n");
	SyncPoint_c(sync_lock_c, sync_cond_c);
	printf("Ciao, sono un thread finto\n");
	
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t vthreads[N_THREADS];
	short int local_port_number; 
	int rc,t,*p;
	pthread_cond_init(&sync_cond_s, NULL);
	pthread_mutex_init(&sync_lock_s, NULL);
	pthread_cond_init(&sync_cond_s, NULL);
	pthread_mutex_init(&sync_lock_c, NULL);
	srand(time(NULL));
	printf("UEUE\n");
	for (t=0; t < N_THREADS; t++){
		p=malloc(sizeof(short int));
		if (p == NULL) {
			perror("malloc fallita\n");
			pthread_exit(NULL);
		}
		*p=5000+t;
	
		rc = pthread_create(&vthreads[t], NULL, Mezzo, p);
		if (rc){
			printf("Errore: %d\n", rc);
			exit(-1);
		}
		else
			printf("Creato thread\n");
	
	}
	SyncPoint_s(sync_lock_s,sync_cond_s);
	/*sleep(5)*/
	for (t=0; t < N_THREADS; t++){
		p=malloc(sizeof(short int));
		if (p == NULL) {
			perror("malloc fallita\n");
			pthread_exit(NULL);
		}
		*p=5000+t;
		rc = pthread_create(&vthreads[t], NULL, STA, p);
		if (rc){
			printf("Errore: %d\n", rc);
			exit(-1);
		}
		else
			printf("Creato thread\n");
	
	}
	SyncPoint_c(sync_lock_c,sync_cond_c);
	printf ("sono il main, muoio\n");
	sleep(5);
	pthread_mutex_destroy(&sync_lock_c);
	printf ("1\n");
	pthread_cond_destroy(&sync_cond_c);
	printf ("2\n");
	pthread_mutex_destroy(&sync_lock_s);
	printf ("3\n");
	pthread_cond_destroy(&sync_cond_s);
	printf ("4\n");
	pthread_exit(NULL);
	printf ("5\n");
	/*ConnessioneTCP(local_port_number);*/
	
	return(0);
}

