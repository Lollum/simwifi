#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/resource.h>

#define CONCRC /**/
/*#define DEBUG_T /**/
/*#define SYNCD /**/
/*#define SLEEP_STA /**/
/*#define DEBUGSERIAL /**/
/*#define DEBUGSERIAL_LOOP /**/
/*#define VERBOSE_M /**/
/*#define VERBOSE_STA /**/
#define VERBOSE_STA_MSG /**/
/*#define CONVERTADDR /**/

#define SOCKET_ERROR   ((int)-1)
#define MASSIMO 2500
#define SIZEBUF MASSIMO
#define MAXSIZE MASSIMO 
#define N_THREADS 5
#define MAX_THREAD 50
#define STA_T N_THREADS
#define MEZZO_T N_THREADS
#define SYNC_MAIN N_THREADS+1
#define SYNC_APP STA_T+1
#define RANDERR 1 /* 0 disattivato */
#define BASELEN 39
#define BROADCAST "FFFFFFFFFFFF"
#define ADDRLEN 13

#define GREEN "\033[0;0;32m"
#define WHITE   "\033[0m"
#define RED "\033[0;0;31m"
#define BLUE "\033[0;0;34m"
#define ORANGE "\033[0;0;33m"

#define DEFAULTCOLOR WHITE
#define CSTA BLUE
#define CMEZZO GREEN
#define CAPP ORANGE

pthread_mutex_t sync_lock_mezzo;
pthread_mutex_t sync_lock_mezzo_m;
pthread_mutex_t sync_lock_sta;
pthread_mutex_t sync_lock_app;
pthread_cond_t sync_cond_mezzo;
pthread_cond_t sync_cond_mezzo_m;
pthread_cond_t sync_cond_sta;
pthread_cond_t sync_cond_app;

pthread_mutex_t mezzo_aux;
pthread_mutex_t Msg_mutex;
pthread_mutex_t init_sta_mutex;
pthread_mutex_t app_mutex;

int sync_count_mezzo=0;
int sync_count_mezzo_1=0;
int sync_count_mezzo_2=0;
int sync_count_mezzo_3=0;
int sync_count_sta=0;
int sync_count_app=0;

int threadSta = STA_T;
int threadMezzo = MEZZO_T;
int syncMain = SYNC_MAIN;
int syncApp = SYNC_APP;

bool messaggioShared;
bool alreadyWaited = false;
bool collision = false;
bool erroriCasuali=false;
bool daEliminare=false;
bool rendomError=false;

bool verboseSta=false;
bool verboseApp=false;
bool verboseMezzo=false;

bool allTest=true;
bool testBroadcast=false;
bool testResponse=false;
bool testCollision=false;
bool testError=false;
char customMsg[2000]="";

typedef struct timeval tempo;

typedef struct{
	pthread_t idApp;
	char addr[13];
} threadAddr;
threadAddr appToStaArray[MAX_THREAD];

typedef struct{
    char v[MAX_THREAD*MAX_THREAD][MASSIMO];
    int top;
} Stack;
typedef Stack Queue;

Queue S;
Queue ackQueue;
Queue msgQueue;

typedef struct{
	char data; /*0 niente, 1 controlla type*/
	char dataType; /*0 ack, 1 msg, 2 rts, 3 cts*/
	char toDS; /*0/1  => 1*/
	char fromDS; /*0/1 => 1*/
	char RTS; /*0*/
	char CTS; /*0*/
	char scan; /*0 no scan, 1 scan, 2 scan response*/
	int duration; /*5,20*/
	int packetlen; /*Codificato in trasmissione*/
	char addr1[13]; 
	char addr2[13]; 
	char addr3[13]; 
	char addr4[13]; 
	char seqCtrl[5]; 
	char payload[2000]; 
	char crc; /*0=errore*/
} Frame;

bool ricevutoAck[MAX_THREAD]; /* controlla se arriva ack*/
char indirizzo[MAX_THREAD][13]; /* assegna numero all'indirizzo reale */
Queue inUscita[MAX_THREAD]; /* messaggi da far mandare dalla STA */
Queue inEntrata[MAX_THREAD];/* messaggi da leggere dalla STA */
Queue daApp[MAX_THREAD];/* messaggi mandati dall'APP alla STA */

/*Funzione ausiliaria per gestire tempo nell'ordine dei millisecondi*/
int milliSleep(unsigned long ms){
    struct timespec time={0};
    time_t sec=(int)(ms/1000);
    ms=ms-(sec*1000);
    time.tv_sec=sec;
    time.tv_nsec=ms*1000000L;
    while(nanosleep(&time,&time)==-1)
    return 1;
}
void waitDuration(int duration){
	milliSleep(duration*100);
}
/*Controlla se e' passata la differenza tra i due archi di tempo*/
int isElapsed(tempo *inizio, tempo *fine, int msDif){
	long dif;
	dif = 1000*(fine->tv_sec - inizio->tv_sec)+(fine->tv_usec - inizio->tv_usec)/1000;
	if (dif >= (long)msDif) return 1;
	else return 0;
}
int randomWait(){
	return (((rand()%2)+2)*2);
}
int randomError(){
	if (rand()%10)
		return 0;
	else
		return 1;
}
/*Genera indirizzo controllando di non passare quello di broadcast*/
void generateAddr(char *addr){
	int i,n;
	char auxaddr[13];
	char partaux[3];
	do {
		i=0;
		while(i<11){
			n=(rand()%254)+1;
			sprintf(partaux,"%02X",n);
			auxaddr[i]=partaux[0];
			auxaddr[i+1]=partaux[1];
			i+=2;
		}
		auxaddr[12]='\0';
	}while (!strcmp(auxaddr,BROADCAST));
	strcpy(addr,auxaddr);
}
/*Funzioni gestione stack-lista*/
void push(Queue *S, char *val){
	if ((S->top) < MAX_THREAD*MAX_THREAD){
		strcpy((S->v[S->top]),val);
		(S->top)++;
	}else{
		printf("Stack pieno, scarto \n");
	}  
}
void pop(char *buffer,Queue *S){	
	strcpy(buffer,"");
    (S->top)--;
    strcpy(buffer,(S->v[S->top]));
}
void stackInit(Queue *S){
    S->top = 0;
}
void stackPrint(Queue *S){
    int i;
    if (S->top == 0)
       printf("Queue vuoto.\n");
    else
    {
       printf("Contenuto Queue: ");
       for (i=0;i<S->top;i++)
       {
          printf("%s && ",S->v[i]); 
       }
       printf("\n");
    }
}
int isEmpty(Queue *S){
	if (S->top == 0){
		return true;
	}else{
		return false;
	}
}
int enQueue(Queue *S, char *val){
	int i;
	Queue aux;
	char buff[MASSIMO];
	stackInit(&aux);
    if (S->top == 0){
       push(S,val);
       return 0;
   }else{
		i=0;
       while (S->top > 0){	
			pop(buff,S);
			push(&aux,buff);
			i++;
       }
		push(&aux,val);
		i=0;
		while (aux.top > 0){	
			pop(buff,&aux);
			push(S,buff);
			i++;
       }
       return 1;
	}
}
void deQueue(char *buff, Queue *S){
	pop(buff,S);
}
void headQueue(char *buff, Queue *S){
	int top;
	strcpy(buff,"");
	top = S->top;
    top--;
    strcpy(buff,(S->v[top]));
}

/*Funzioni di conversione per la codifica di SeqCtrl*/
int frameToSCtrl(char *input){
	return (int)strtol(input, NULL, 10);
}
void sctrlToFrame(char *output,int input){
	sprintf(output,"%d",input);
}

/*Funzioni di conversione per la codifica degli indirizzi*/
void frameToAddr(char *output, char *input){
	int buff[6],i,aux;
	#ifdef CONVERTADDR
	printf("frametoaddr cosa sto leggendo: %s \n",input);
	#endif
	for (i=0;i<6;i++){
		aux=(int)input[i];
		if (aux<0){
			aux=256+aux;
		}
		buff[i]=aux;
	}
	sprintf(output,"%02X%02X%02X%02X%02X%02X",buff[5],buff[4],buff[3],buff[2],buff[1],buff[0]);
	#ifdef CONVERTADDR
	printf("frametoaddr: %s len %d\n",output,strlen(output));
	#endif
}
void addrToFrame(char *output, char *input){
	int i;
	char coppia[3];
	char a,b;
	char out[7];
	int j=0;
	#ifdef CONVERTADDR
	printf("addrtoframe: cosa sto leggendo: %s \n",input);
	#endif
	for (i=11;i>=0;i--){
		if ((i%2)==1){
			a=input[i];
		}else{
			b=input[i];
			sprintf(coppia,"%c%c",b,a);
			out[j]=(char)strtol(coppia, NULL, 16);
			j++;
		}
		
	}
	out[j]='\0';
	#ifdef CONVERTADDR
	printf("addrtoframe: %s len %d\n",out,strlen(out));
	#endif
	strcpy(output,out);
}
/*Funzioni di coversione per la codifica della lunghezza del pacchetto*/
int frameToPktLen(char *input){
	int buff[2],i,aux;
	char output[5];
	for (i=0;i<2;i++){
		aux=(int)input[i];
		if (aux<0){
			aux=256+aux;
		}
		buff[i]=aux;
	}
	if (buff[1]==255) buff[1]=0;
	sprintf(output,"%02X%02X",buff[1],buff[0]);
	return (int)strtol(output, NULL, 16);
}
void pktlenToFrame(char *output, int input){
	char buff[5];
	int i;
	char coppia[3];
	char a,b;
	char out[3];
	int j=0;
	sprintf(buff,"%04X",input);
	for (i=3;i>=0;i--){
		if ((i%2)==1){
			a=buff[i];
		}else{
			b=buff[i];
			coppia[0]=b;
			coppia[1]=a;
			coppia[2]='\0';
			out[j]=(char)strtol(coppia, NULL, 16);
			j++;
		}
		
	}

	if (out[1]==((char)strtol("00", NULL, 16))) 
		out[1]=(char)strtol("FF", NULL, 16);
	out[2]='\0';
	strcpy(output,out);
}
/*Funzioni per la conversione della codifica della durata del pacchetto*/
char durationToFrame(int input){
	char buff[5];
	sprintf(buff,"%02X",input);
	return (char)strtol(buff, NULL, 16);	
}
int frameToDuration(char input){
	int buff,aux;
	char output[3];
	aux=(int)input;
	if (input<0){
		aux=256+aux;
	}
	buff=aux;
	sprintf(output,"%02X",buff);
	return (int)strtol(output, NULL, 16);	
}
/*Funzioni serializzazione e de-serializzazione del frame*/
void frameSerialize(char *output,Frame frame){
	char addr[4][7];
	char pl[3];
	addrToFrame(addr[0],frame.addr1);
	addrToFrame(addr[1],frame.addr2);
	addrToFrame(addr[2],frame.addr3);
	addrToFrame(addr[3],frame.addr4);
	pktlenToFrame(pl,frame.packetlen);
	sprintf(output,
	"%c%c%c%c%c%c%c%c%s%s%s%s%s%s%s%c"
	,frame.data,frame.dataType,frame.toDS,frame.fromDS,frame.RTS,frame.CTS,frame.scan,durationToFrame(frame.duration),pl,
	addr[0],addr[1],addr[2],addr[3],frame.seqCtrl,frame.payload,frame.crc);
}
void frameDeserialize(Frame *output, char *input){
	Frame aux;
	int i,j;
	int pck,ad1,ad2,ad3,ad4,sqc,pld;
	char addr[4][7];
	char pl[3];
	pck=8;
	ad1=pck+2;
	ad2=ad1+6;
	ad3=ad2+6;
	ad4=ad3+6;
	sqc=ad4+6;
	pld=sqc+4;
	aux.data=input[0];
	aux.dataType=input[1];
	aux.toDS=input[2];
	aux.fromDS=input[3];
	aux.RTS=input[4];
	aux.CTS=input[5];
	aux.scan=input[6];
	aux.duration=frameToDuration(input[7]);
	j=0;
	strcpy(pl,"");
	for (i=pck;i<=ad1-1;i++){
		pl[j]=input[i];
		j++;
	}
	pl[j]='\0';
	aux.packetlen=frameToPktLen(pl);
	j=0;
	strcpy(addr[0],"");
	for (i=ad1;i<=ad2-1;i++){
		addr[0][j]=input[i];
		j++;
	}
	addr[0][j]='\0';
	frameToAddr(aux.addr1,addr[0]);
	j=0;
	strcpy(addr[1],"");
	for (i=ad2;i<=ad3-1;i++){
		addr[1][j]=input[i];
		j++;
	}
	addr[1][j]='\0';
	frameToAddr(aux.addr2,addr[1]);
	j=0;
	strcpy(addr[2],"");
	for (i=ad3;i<=ad4-1;i++){
		addr[2][j]=input[i];
		j++;
	}
	addr[2][j]='\0';
	frameToAddr(aux.addr3,addr[2]);
	j=0;
	strcpy(addr[3],"");
	for (i=ad4;i<=sqc-1;i++){
		addr[3][j]=input[i];
		j++;
	}
	addr[3][j]='\0';
	frameToAddr(aux.addr4,addr[3]);
	j=0;
	strcpy(aux.seqCtrl,"");
	for (i=sqc;i<=pld-1;i++){
		aux.seqCtrl[j]=input[i];
		j++;
	}
	aux.seqCtrl[j]='\0';
	j=0;
	strcpy(aux.payload,"");
	for (i=pld;i<=strlen(input)-2;i++){
		aux.payload[j]=input[i];
		j++;
	}
	aux.payload[j]='\0';
	aux.crc=input[strlen(input)-1];
	*output=aux;
}
/*Funzione per settare write e read non bloccanti*/
int set_socket_non_blocking(int socketfd){
	int flags;
	if ((flags=fcntl(socketfd, F_GETFL,0))<0) return (0);
	flags |= O_NONBLOCK;
	if (fcntl(socketfd,F_SETFL,flags)<0) return (0);
	return(1);
}
/*Funzione di sincronizzazione tra i thread*/
void SyncPoint(pthread_mutex_t *sync_lock_t, pthread_cond_t *sync_cond_t,short int sync_max,int *sync_count){ 
   pthread_mutex_lock(sync_lock_t); 
   (*sync_count)++; 
   if (*sync_count < sync_max) {
         #ifdef SYNCD
         printf("Aspetto! counter a %d\n",*sync_count);
         #endif 
         pthread_cond_wait(sync_cond_t, sync_lock_t); 
	 }
   else    {
         #ifdef SYNCD
         printf("Ci Siamo TUTTI! mezzo \n");
         #endif
         pthread_cond_broadcast(sync_cond_t); 	 
   }

   #ifdef SYNCD
   printf("Sblocco Mutex! server\n");
   #endif
   (*sync_count)= 0;
   pthread_mutex_unlock (sync_lock_t);
   return; 
} 
void printHelp(){
	printf("Modalita Verbose: \n -vsta : thread STA \n -vmezzo : thread Mezzo \n -vapp : thread App\n");
	printf("Opzioni Test; Di base sono attivi i 4 test, Broadcast, Response, Collision, Error.\nIn caso venga selezionato un test specifico gli altri verrano disattivati, eslcuso nel caso vengano specificati:\n-tbroadcast\n-tresponse\n-tcollision\n-terror\n");
	printf("Messaggio specifico: \n-msg \"messaggio\".\nVerra' inviato prima degli altri test, dal primo thread App all'ultimo.\n");
	printf("Numero thread; E' possibile specificare il numero di thread:\n -threads #numero\nAttenzione, il numero di thread effettivi sara' triplicato [Mezzo-Sta-App]\n");
}
/*Funzione ausiliaria per creare uno scheletro di un frame*/
void creaMessaggio(Frame *output,char *addrto, char *addrfrom, char *message, int len){
	output->data='1';
	output->dataType='1';
	output->toDS='1';
	output->fromDS='1';
	output->RTS='0';
	output->CTS='0';
	output->scan='0';
	output->duration=5;
	strcpy(output->addr1,addrto);
	strcpy(output->addr2,addrfrom);
	strcpy(output->addr3,addrto);
	strcpy(output->addr4,addrfrom);
	strcpy(output->seqCtrl,"0000");
	strcpy(output->payload,message);
	output->crc='1';
	output->packetlen=BASELEN+len;
	if (output->packetlen <= 100){
		output->duration=5;
	}else{
		output->duration=20;
	}
}
/*Funzione che modifica SeqCtrl SENZA de-serializzare*/
void frameModSctrl(char *frame, int counter){
	char buff[10]="";
	int i,j;
	sprintf(buff,"%04d",counter);
	j=0;
	for (i=24;i<28;i++){
		frame[i]=buff[j];
		j++;
	}
}
/*Funzione che modifica Collision SENZA de-serializzare*/
void setCollision(char *msg, char value){
	if (value)
		msg[strlen(msg)-1]='0';
}
/*Singolo Thread del mezzo*/
void *Mezzo(void *param){
	bool isConnected,hoMessaggio,sendAck,sendMsg,daMandare,hoIndirizzo,sonDentro;
	struct sockaddr_in Local, Cli;
	short int socketfd, local_port_number;
	int newsocketfd1, OptVal, ris,readerr;
	unsigned int len;
	int n, nread, nwrite,i;
	char buf_in[MAXSIZE];
	char buf_out[MAXSIZE];
	char mySTAaddr[13]="";
	Frame incoming_mex;
	Frame send_mex;
	local_port_number = *((short int*) param);
	free(param);
	
	/* ##### inizializzazione connessione #####*/
	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketfd == SOCKET_ERROR) {
		printf (CMEZZO"socket() failed, Err: %d \"%s\"\n" DEFAULTCOLOR, errno,strerror(errno) );
	}
	OptVal = 1;
	if (verboseMezzo) printf ("setsockopt()\n");
	ris = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
	if (ris == SOCKET_ERROR)  {
		printf (CMEZZO"setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n" DEFAULTCOLOR, errno,strerror(errno));
	}
	memset ( &Local, 0, sizeof(Local) );
	Local.sin_family		=	AF_INET;
	Local.sin_addr.s_addr	=	htonl(INADDR_ANY);
	Local.sin_port		=	htons(local_port_number);
	if (verboseMezzo) printf ("bind()\n");
	ris = bind(socketfd, (struct sockaddr*) &Local, sizeof(Local));
	if (ris == SOCKET_ERROR)  {
		printf (CMEZZO"bind() failed, Err: %d \"%s\"\n" DEFAULTCOLOR,errno,strerror(errno));
	}
	if (verboseMezzo) printf ("listen()\n");
	ris = listen(socketfd, 10 );
	if (ris == SOCKET_ERROR)  {
		printf (CMEZZO"listen() failed, Err: %d \"%s\"\n" DEFAULTCOLOR ,errno,strerror(errno));
	}
	SyncPoint(&sync_lock_mezzo,&sync_cond_mezzo,syncMain,&sync_count_mezzo);
	
	/*######## In attesa connessione della stazione stazione###########*/
	do {
		memset ( &Cli, 0, sizeof(Cli) );
		len=sizeof(Cli);	
		newsocketfd1 = accept(socketfd, (struct sockaddr*) &Cli, &len);
		} while( (newsocketfd1<0)&&(errno==EINTR) );
	if (newsocketfd1 == SOCKET_ERROR)  {
		printf (CMEZZO"accept() failed, Err: %d \"%s\"\n" DEFAULTCOLOR,errno,strerror(errno));
		/*return SOCKET_ERROR;*/
	}
	if (verboseMezzo) printf(CMEZZO"connection from %s : %d\n" DEFAULTCOLOR, inet_ntoa(Cli.sin_addr), ntohs(Cli.sin_port));
	isConnected = true;
	set_socket_non_blocking(newsocketfd1);
	hoMessaggio=false;
	daMandare=false;
	sendAck=false;
	sendMsg=false;
	daEliminare=false;
	hoIndirizzo=false;
	sonDentro=false;

	/********** loop principale************/
	while (isConnected){
		nread=0;
		strcpy(buf_in,"");
		/* Leggo messaggio se disponibile */
		for (i=0;i<5;i++){
			while( (n=read(newsocketfd1, &(buf_in[nread]), MASSIMO )) >0) {
				if (verboseMezzo) printf(CMEZZO"Mezzo: %s ricevo messaggio %s,n:%d\n"DEFAULTCOLOR,mySTAaddr,buf_in,n);
				fflush(stdout);
				nread+=n;
				hoMessaggio = true;
				if(buf_in[nread-1]=='\0') break; /* fine stringa */
			}
			readerr = errno;
			if(n<=0) {
				if ((readerr == EWOULDBLOCK)) {
					}
				else{
					if (readerr != 0){
						char msgerror[1024];
						sprintf(msgerror,"read() failed [err %d] "DEFAULTCOLOR,readerr);
						perror(msgerror);
						exit(1); 
					}
					n=close(newsocketfd1);
					isConnected = false;
				}
			}
			milliSleep(10);
		}
		/******************************** sincronizza *******************************
		 *Se e' stato letto un messaggio, dopo la sincronizzazione i mezzi si occupano di 
		 * mettere i messaggi in arrivo in coda da ritrasmettere.
		 */
		SyncPoint(&sync_lock_mezzo,&sync_cond_mezzo,threadMezzo,&sync_count_mezzo_2);
		if (hoMessaggio){
			frameDeserialize(&incoming_mex,buf_in);
			if (!hoIndirizzo)
				strcpy(mySTAaddr,incoming_mex.addr2);
			if (pthread_mutex_trylock(&mezzo_aux) == 0){
				sonDentro=true;
				waitDuration(incoming_mex.duration);
			}else{
				pthread_mutex_lock(&Msg_mutex);
				collision=true;
				pthread_mutex_unlock(&Msg_mutex);
			};
			pthread_mutex_lock(&Msg_mutex);
			if (!sonDentro) waitDuration(incoming_mex.duration);
			if (collision)
					incoming_mex.crc='0';
				else 
					if (erroriCasuali && randomError()) {
						incoming_mex.crc='0';
						printf(RED"MEZZO: %s, purtroppo un elettrone si sveglia tardi e causa un errore!\n"DEFAULTCOLOR,mySTAaddr);
					}
			pthread_mutex_unlock(&Msg_mutex);
			if (incoming_mex.dataType=='0' || incoming_mex.dataType=='3'){
				if (verboseMezzo) printf(ORANGE"Mezzo: %s ho ack\n"DEFAULTCOLOR,mySTAaddr);
				strcpy(buf_in,"");
				frameSerialize(buf_in,incoming_mex);
				pthread_mutex_lock(&Msg_mutex);
				enQueue(&ackQueue,buf_in);
				pthread_mutex_unlock(&Msg_mutex);
			}else{
				if (verboseMezzo) printf(ORANGE"Mezzo: %s ho msg\n"DEFAULTCOLOR,mySTAaddr);
				strcpy(buf_in,"");
				frameSerialize(buf_in,incoming_mex);
				pthread_mutex_lock(&Msg_mutex);
				enQueue(&msgQueue,buf_in);
				pthread_mutex_unlock(&Msg_mutex);
			}
		}else{
			
		}
		/******************************** sincronizza *******************************
		 * Mezzi leggono eventuali messaggi/ack da trasmettere e procedono a trasmettere
		 * alla propria stazione di competenza
		 */
		SyncPoint(&sync_lock_mezzo_m,&sync_cond_mezzo_m,threadMezzo,&sync_count_mezzo_1);
		strcpy(buf_out,"");
		if (!isEmpty(&ackQueue)){
			headQueue(buf_out,&ackQueue);
			frameDeserialize(&send_mex,buf_out);
			if (verboseMezzo) printf(CMEZZO"Mezzo: %s letto ack: %s \n"DEFAULTCOLOR,mySTAaddr, buf_out);
			sendAck=true;
			daEliminare=true;
			if (!strcmp(send_mex.addr2,mySTAaddr)){
				daMandare=false;
			}else{
				daMandare=true;
			}
		}else{
			if (!isEmpty(&msgQueue)){
				headQueue(buf_out,&msgQueue);
				frameDeserialize(&send_mex,buf_out);
				if (verboseMezzo) printf(CMEZZO"Mezzo: %d letto msg: %s \n"DEFAULTCOLOR,local_port_number, buf_out);
				sendMsg=true;
				daEliminare=true;
				if (!strcmp(send_mex.addr2,mySTAaddr)){
					daMandare=false;
				}else{
					daMandare=true;
				}
			}else{
				daMandare=false;
			}				
		}
		if (daMandare){
			if (!hoMessaggio) waitDuration(send_mex.duration);
			nwrite=0;
			len=strlen(buf_out)+1;
			pthread_mutex_lock(&Msg_mutex);
			setCollision(buf_out,collision);
			pthread_mutex_unlock(&Msg_mutex);
			while(((n=write(newsocketfd1, &(buf_out[nwrite]), len-nwrite)) >0)){
				nwrite+=n;
				if (verboseMezzo) printf(CMEZZO"******Mezzo: %d mando messaggio: %s \n"DEFAULTCOLOR,local_port_number, buf_out);
			}
			if(n<0 && isConnected) {
				char msgerror[1024];
				sprintf(msgerror,"mezzo: write() failed [err %d] ",errno);
				perror(msgerror);
			}
		}
		/******************************** sincronizza *******************************
		 * tutti i mezzi hanno trasmesso il messaggio, ci si assicura che abbiano finito
		 */
		if (sonDentro){ 
			pthread_mutex_unlock(&mezzo_aux);
			sonDentro=false;
		}
		SyncPoint(&sync_lock_mezzo,&sync_cond_mezzo,threadMezzo,&sync_count_mezzo_2);
		if ((daEliminare)&&(pthread_mutex_trylock(&mezzo_aux) == 0)){
			if (sendAck){
				deQueue(buf_out,&ackQueue);
			}
			if (sendMsg){
				deQueue(buf_out,&msgQueue);
			}
			pthread_mutex_unlock(&mezzo_aux);
		}
		alreadyWaited=false;
		daMandare=false;
		hoMessaggio = false;
		pthread_mutex_lock(&Msg_mutex);
		collision=false;
		pthread_mutex_unlock(&Msg_mutex);
		sendMsg=false;
		sendAck=false;
		daEliminare=false;
		sonDentro=false;
		/******************************** sincronizza *******************************
		 * Ciclo finito, si ricomincia
		 */
		SyncPoint(&sync_lock_mezzo_m,&sync_cond_mezzo_m,threadMezzo,&sync_count_mezzo_3);
	} 
	close(newsocketfd1);
	close(socketfd);
	pthread_exit(NULL);
}
/*Funzione che permette ad un applicazione di mandare un messaggio tramite la sua stazione*/
int Send(char *macDest, char *buffer, int length){
	int i=0;
	int j=0;
	int wait;
	Frame aux;
	bool ack=false;
	bool trovato=false;
	clock_t tInizio, tFine;
	char buf[MASSIMO];
	char addrFrom[13];
	if (length>70)
		wait=4;
	else
		wait=6;
	pthread_mutex_lock(&app_mutex);
	while (!(pthread_self() == appToStaArray[j].idApp)){
		j++;
	}
	strcpy(addrFrom,appToStaArray[j].addr);
	pthread_mutex_unlock(&app_mutex);
	i=0;
	while ((i<threadSta)&&!trovato){
		if (!strcmp(indirizzo[i],addrFrom)){
			trovato=true;
			break;
		}
		i++;
	}
	if (!trovato) return false;
	creaMessaggio(&aux,macDest,addrFrom,buffer,length);
	pthread_mutex_lock(&init_sta_mutex);
	frameSerialize(buf,aux);
	enQueue(&daApp[i],buf);
	ricevutoAck[i]=false;
	pthread_mutex_unlock(&init_sta_mutex);
	tInizio = time(NULL);
	if (!strcmp(macDest,BROADCAST)) return true;
	while(difftime(tFine,tInizio)<wait){
		milliSleep(100); /* solo per non bombardare di richieste */
		tFine = time(NULL);
		pthread_mutex_lock(&init_sta_mutex);
		ack=ricevutoAck[i];
		pthread_mutex_unlock(&init_sta_mutex);
		if (ack) {
			return true;
		}
	}
	return false;
}
/*Funzione che permette ad un applicazione di leggere un messaggio tramite la sua stazione*/
int Recv(char *macRecv, char *buffer, int *length){
	char aux[MASSIMO];
	Frame auxFrame;
	int j=0;
	pthread_mutex_lock(&app_mutex);
	while (!(pthread_self() == appToStaArray[j].idApp)){
		j++;
	}
	pthread_mutex_unlock(&app_mutex);
	pthread_mutex_lock(&init_sta_mutex);
	if (isEmpty(&inEntrata[j])){
		pthread_mutex_unlock(&init_sta_mutex);
		return false;
	}else{
	deQueue(aux,&inEntrata[j]);
	pthread_mutex_unlock(&init_sta_mutex);
	frameDeserialize(&auxFrame,aux);
	strcpy(macRecv,auxFrame.addr2);
	strcpy(buffer,auxFrame.payload);
	*length=auxFrame.packetlen-BASELEN+1;
	return true;
	}
}
/*Singolo thread Applicazione*/
void *App(void *param){
	char staAddr[13]="";
	char buffer[MASSIMO]="";
	char fromAddr[13]="";
	char buffermsg1[2000]="I wanted to be... a lumberjack!";
	char buffermsg2[2000]="He's a lumberjack, and he's okay.";
	char buffermsg3[2000];
	char buffermsg4[2000]="He sleeps all night and he works all day.";
	char bufferCustom[2000]="";
	int len=10;
	int i,numberSta;
	for(i=0;i<2000;i++) buffermsg3[i]='i';
	buffermsg3[1999]='\0';
	numberSta=*((unsigned int*) param);
	pthread_mutex_lock(&app_mutex);
	strcpy(staAddr,indirizzo[numberSta]);
	appToStaArray[numberSta].idApp=pthread_self();
	strcpy(appToStaArray[numberSta].addr,staAddr);
	strcpy(bufferCustom,customMsg);
	pthread_mutex_unlock(&app_mutex);
	strcat(buffermsg1,staAddr);
	strcat(buffermsg2,staAddr);
	strcat(buffermsg4,staAddr);
	if (verboseApp) printf("Applicazione della stazione con indirizzo: %s\n",staAddr);
	free(param);
	/************************TEST MESSAGGIO CUSTOM*******************************/
	if(strcmp(bufferCustom,"")){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		if (numberSta<1){
			Send(appToStaArray[threadSta-1].addr,bufferCustom,strlen(bufferCustom));
		}
		milliSleep(2000);
		while (Recv(fromAddr,buffer,&len))
			printf(CAPP"Sono l'applicazione della STA: %s ho ricevuto da %s Messaggio: %s Len: %d\n"DEFAULTCOLOR,staAddr,fromAddr,buffer,len);	
	}
	/************************TEST BROADCAST*******************************/
	if(allTest || testBroadcast){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		milliSleep(2000);
		if (numberSta<1){
			Send(BROADCAST,buffermsg1,strlen(buffermsg1));
		}
		milliSleep(2000);
		while (Recv(fromAddr,buffer,&len))
			printf(CAPP"Sono l'applicazione della STA: %s ho ricevuto da %s Messaggio: %s Len: %d\n"DEFAULTCOLOR,staAddr,fromAddr,buffer,len);
	}
	/***************************TEST RISPOSTA, SENZA COLLISIONI****************************/
	if(allTest || testResponse){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		milliSleep((numberSta)*2050);
		Send(appToStaArray[0].addr,buffermsg2,strlen(buffermsg2));
		milliSleep(threadSta*2000);
		while (Recv(fromAddr,buffer,&len))
			printf(CAPP"Sono l'applicazione della STA: %s ho ricevuto da %s Messaggio: %s Len: %d\n"DEFAULTCOLOR,staAddr,fromAddr,buffer,len);	
	}
	/*************************TEST COLLISIONI******************************/
	if(allTest || testCollision){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		Send(appToStaArray[0].addr,buffermsg3,strlen(buffermsg3));
		milliSleep(threadSta*2000);
	}
	/*************************ERRORI CASUALI******************************/
	if(allTest || testError){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		while(1){
			if (numberSta<1){
				Send(appToStaArray[threadSta-1].addr,buffermsg4,strlen(buffermsg4));
			}
		}
	}
	printf(RED"Applicazione terminata!\n");
	pthread_exit(NULL);
}
/*Singolo thread Stazione*/
void *Sta(void *param){
	struct sockaddr_in Local, Serv;
	char string_remote_ip_address[100]="127.0.0.1";
	short int remote_port_number;
	unsigned int *p;
	int socketfd, OptVal, ris;
	int n, nread, nwrite, len, numIndirizzo,rc,seqCtrl;
	char mySTAaddr[13]="100";
	bool devoMandareAck,devoMandareMessaggio,okAck,okErrore;
	char msg_buffer_in[MAXSIZE];
	char msg_buffer_out[MAXSIZE];
	Queue ackDaMandare;
	pthread_t appThread;
	Frame ackStandard;
	Frame frameBuffer;
	tempo istInvioErrore,istAttualeErrore,istInvioAck,istAttualeAck;
	bool newMsg=false;
	/****Inizio inizializzazione****/
	remote_port_number = *((short int*) param);
	free(param);
	generateAddr(mySTAaddr);
	printf(CSTA"Ho generato l'indirizzo, chiamatemi: %s\n"DEFAULTCOLOR,mySTAaddr);
	numIndirizzo=remote_port_number-5000;
	pthread_mutex_lock(&init_sta_mutex);
	strcpy(indirizzo[numIndirizzo],mySTAaddr);
	pthread_mutex_unlock(&init_sta_mutex);
	if (verboseSta) printf("indirizzo numero %d:%s\n",numIndirizzo,indirizzo[numIndirizzo]);
	stackInit(&ackDaMandare);
	
	/* inizializzo ack standard */
	creaMessaggio(&ackStandard,"123456789ABC",mySTAaddr,"",0);
	ackStandard.dataType='0';
	
	if (verboseSta) printf(ORANGE"STA:%d indirizzo %s\n"DEFAULTCOLOR,remote_port_number,mySTAaddr);
	socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketfd == SOCKET_ERROR) {
		printf (CSTA"socket() failed, Err: %d \"%s\"\n" DEFAULTCOLOR, errno,strerror(errno));
	}

	OptVal = 1;
	ris = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
	if (ris == SOCKET_ERROR)  {
		printf (CSTA"setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n"DEFAULTCOLOR, errno,strerror(errno));
	}
	memset ( &Local, 0, sizeof(Local) );
	Local.sin_family		=	AF_INET;
	Local.sin_addr.s_addr	=	htonl(INADDR_ANY);         /* wildcard */
	Local.sin_port	=	htons(0);
	ris = bind(socketfd, (struct sockaddr*) &Local, sizeof(Local));
	if (ris == SOCKET_ERROR)  {
		printf (CSTA"bind() failed, Err: %d \"%s\"\n"DEFAULTCOLOR,errno,strerror(errno));
		exit(1);
	}
	memset ( &Serv, 0, sizeof(Serv) );
	Serv.sin_family	 =	AF_INET;
	Serv.sin_addr.s_addr  =	inet_addr(string_remote_ip_address);
	Serv.sin_port		 =	htons(remote_port_number);
	SyncPoint(&sync_lock_sta,&sync_cond_sta,syncMain,&sync_count_sta);
	ris = connect(socketfd, (struct sockaddr*) &Serv, sizeof(Serv));
	if (ris == SOCKET_ERROR)  {
		printf (CSTA"connect() failed, Err: %d \"%s\"\n"DEFAULTCOLOR,errno,strerror(errno));
		exit(1);
	}
	if (verboseSta) printf (CSTA"STA %d dopo connect()\n"DEFAULTCOLOR,remote_port_number);
	fflush(stdout);
	
	p=malloc(sizeof(unsigned int));
	/*Creo thread app personale*/
	if (p == NULL) {
		perror("malloc fallita\n");
	}
	
	*p=numIndirizzo;
	rc = pthread_create(&appThread, NULL, App, p);
	if (rc){
		printf("Errore: %d\n", rc);
	}else{
	}
	
	set_socket_non_blocking(socketfd);
	/****Fine inizializzazione****/
	messaggioShared=false;
	devoMandareMessaggio=false;
	devoMandareAck=false;
	
	okAck=true;
	okErrore=true;
	
	seqCtrl=0;
	gettimeofday(&istInvioAck,NULL);
	gettimeofday(&istInvioErrore,NULL);
	/*Ciclo principale*/
	while(1){
		fflush(stdout);
		nwrite=0;
		/*Controllo che non debba lasciare il canale libero per ack di altra stazione
		 * o che non abbia un messaggio in attesa di ack*/
		gettimeofday(&istAttualeAck,NULL);
		gettimeofday(&istAttualeErrore,NULL);
		if (isElapsed(&istAttualeAck,&istInvioAck,1000)) okAck=true;
		if (isElapsed(&istAttualeAck,&istAttualeErrore,randomWait()*1000)) okErrore=true;
		/*In base al controllo precedente guardo cosa devo mandare, sempre se 
		 * devo mandare qualcosa*/
		if (!isEmpty(&ackDaMandare) && okAck){
				deQueue(msg_buffer_out,&ackDaMandare);
				devoMandareAck=true;
		}else{
			if ((!isEmpty(&inUscita[numIndirizzo]))&& okAck){
				pthread_mutex_lock(&init_sta_mutex);
				deQueue(msg_buffer_out,&inUscita[numIndirizzo]);
				pthread_mutex_unlock(&init_sta_mutex);
				frameDeserialize(&frameBuffer,msg_buffer_out);
				if (!strcmp(frameBuffer.addr1,mySTAaddr))
					devoMandareMessaggio=false;
				else
					devoMandareMessaggio=true;
			}else{
				if((!isEmpty(&daApp[numIndirizzo])) && okAck && okErrore){
				pthread_mutex_lock(&init_sta_mutex);
				deQueue(msg_buffer_out,&daApp[numIndirizzo]);
				pthread_mutex_unlock(&init_sta_mutex);
				frameDeserialize(&frameBuffer,msg_buffer_out);	
				if (!strcmp(frameBuffer.addr1,mySTAaddr))
					devoMandareMessaggio=false;
				else
					devoMandareMessaggio=true;
				}
			}
		}
		/*Invio del messaggio, se disponibile*/
		len = strlen(msg_buffer_out)+1;
		if ((devoMandareAck || devoMandareMessaggio)){
			gettimeofday(&istInvioErrore,NULL);
			frameModSctrl(msg_buffer_out,seqCtrl++);
			while( (n=write(socketfd, &(msg_buffer_out[nwrite]), len-nwrite)) >0 ){
				nwrite+=n;
				devoMandareMessaggio=false;
				devoMandareAck=false;
				if (verboseSta) printf(ORANGE"STA %s, invio messaggio %s\n"DEFAULTCOLOR,mySTAaddr,msg_buffer_out);
			}
			if(n<0) {
				char msgerror[1024];
				sprintf(msgerror,"write() failed [err %d] ",errno);
				perror(msgerror);
				fflush(stdout);
			}
		}
		nread=0;
		fflush(stdout);
		/*Continuo a leggere messaggi dal mezzo, in ogni caso*/
		while(((n=read(socketfd, &(msg_buffer_in[nread]), MAXSIZE )) >0)){	
			if (verboseSta) printf(ORANGE"STA: %s ricevo messaggio %s,n:%d\n"DEFAULTCOLOR,mySTAaddr,msg_buffer_in,n);
			nread+=n;
			fflush(stdout);		
			newMsg=true;
			if(msg_buffer_in[nread-1]=='\0') break; /* fine stringa */
		}
		if(n<0) {
			if ((errno == EWOULDBLOCK)){
				if (!newMsg){
					#ifdef VERBOSE_STA_K
					printf(CSTA"STA %d nessuno Messaggio \n" DEFAULTCOLOR,remote_port_number);
					#endif
				}
			}else{
				if (errno != 0){
					char msgerror[1024];
					sprintf(msgerror,"read() failed [err %d] ",errno);
					perror(msgerror); 
				}
				n=close(socketfd);
			}
		}
		/*Controllo il messaggio, se disponibile, controllando destinatario e tipologia*/
		if (newMsg){
			frameDeserialize(&frameBuffer,msg_buffer_in);
			if ((!strcmp(frameBuffer.addr1,mySTAaddr)) || (!strcmp(frameBuffer.addr1,BROADCAST))){
				#ifdef CONCRC
				if(frameBuffer.crc=='1'){
				#endif
					if (frameBuffer.dataType=='1' || frameBuffer.dataType=='2'){
						#ifdef VERBOSE_STA_MSG
						printf(CSTA"STA: Sono %d:%s, ho letto il messaggio di %s pacchetto: %d\n"DEFAULTCOLOR, remote_port_number,mySTAaddr,frameBuffer.addr2,frameBuffer.packetlen);
						#endif
						strcpy(ackStandard.addr1,frameBuffer.addr2);
						strcpy(ackStandard.addr3,frameBuffer.addr2);
						pthread_mutex_lock(&init_sta_mutex);
						enQueue(&inEntrata[numIndirizzo],msg_buffer_in);
						pthread_mutex_unlock(&init_sta_mutex);
						frameSerialize(msg_buffer_out,ackStandard);
						if (strcmp(frameBuffer.addr1,BROADCAST)) enQueue(&ackDaMandare,msg_buffer_out);
					}else{
						#ifdef VERBOSE_STA_MSG
						printf(CSTA"STA: Sono %d:%s, ricevo l'ack di %s pacchetto: %d\n"DEFAULTCOLOR, remote_port_number,mySTAaddr,frameBuffer.addr2,frameBuffer.packetlen);
						#endif
						pthread_mutex_lock(&init_sta_mutex);
						ricevutoAck[numIndirizzo]=true;
						pthread_mutex_unlock(&init_sta_mutex);
						okErrore=true;
					}
				#ifdef CONCRC
				}else{
					#ifdef VERBOSE_STA_MSG
					printf(CSTA"STA: Sono %d:%s, ho letto il messaggio di %s, ma c'e' errore pacchetto: %d\n"DEFAULTCOLOR, remote_port_number,mySTAaddr, frameBuffer.addr2,frameBuffer.packetlen);
					#endif
				}
				#endif
			}else{
				gettimeofday(&istAttualeAck,NULL);
			}
			newMsg = false;
		}
		strcpy(msg_buffer_in,"");
	}

  close(socketfd);
  pthread_exit(NULL);
}
/*Corpo principale e gestore*/
int main(int argc, char *argv[]){
	pthread_t sta_threads[MAX_THREAD],mezzo_threads[MAX_THREAD];
	short int i,r; 
	int rc,t,*p,thread,wt;
	r=1;
	/*Leggo opzioni*/
	while ((argc>1)&&(r < argc)) {
		if (!strcmp(argv[r],"-help")) {
			printHelp();
			exit(1);
		}
		if (!strcmp(argv[r],"-vmezzo")) verboseMezzo=true;
		if (!strcmp(argv[r],"-vsta")) verboseSta=true;
		if (!strcmp(argv[r],"-vapp")) verboseApp=true;
		if (!strcmp(argv[r],"-vapp")) verboseApp=true;
		if (!strcmp(argv[r],"-tbroadcast")){
			allTest=false;
			testBroadcast=true;
		}
		if (!strcmp(argv[r],"-tresponse")){
			allTest=false;
			testResponse=true;
		}
		if (!strcmp(argv[r],"-tcollision")){
			allTest=false;
			testCollision=true;
		}
		if (!strcmp(argv[r],"-terror")){
			allTest=false;
			testError=true;
		}
		if (!strcmp(argv[r],"-msg")){
			if (++r>= argc){
				printf("Errore: nessun messaggio\n");
				exit(1);
			}else{
				if (argv[r][0]=='-'){
					printf("Errore: nessun messaggio\n");
					exit(1);
				}
				else 
					strcpy(customMsg,argv[r]);
			}
		}
		if (!strcmp(argv[r],"-threads")){
			if (++r>= argc){
				printf("Errore: numero di thread non valido\n");
				exit(1);
			}
			else {
				thread=strtol(argv[r],NULL,10);
				if ((thread>0) && (thread<=50)){
					threadSta=threadMezzo=strtol(argv[r],NULL,10);
					syncApp=syncMain=threadMezzo+1;
				}
				
			}
		}
		r++;
	}
	printf("Messaggio: %s, threads: %d\n",customMsg,threadSta);
	/*Inizializzo condizioni e mutex*/
	pthread_cond_init(&sync_cond_mezzo, NULL);
	pthread_mutex_init(&sync_lock_mezzo, NULL);
	pthread_cond_init(&sync_cond_mezzo_m, NULL);
	pthread_mutex_init(&sync_lock_mezzo_m, NULL);
	
	pthread_cond_init(&sync_cond_sta, NULL);
	pthread_mutex_init(&sync_lock_sta, NULL);
	
	pthread_mutex_init(&sync_lock_app,NULL);
	pthread_mutex_init(&sync_lock_app, NULL);
	
	pthread_mutex_init(&Msg_mutex, NULL);
	pthread_mutex_init(&init_sta_mutex, NULL);
	pthread_mutex_init(&app_mutex,NULL);
	pthread_mutex_init(&mezzo_aux,NULL);
	
	srand(time(NULL));
	/*inizializzo liste*/
	stackInit(&S);
	stackInit(&ackQueue);
	stackInit(&msgQueue);
	for (i=0;i<threadSta;i++){
		stackInit(&inUscita[i]);
		stackInit(&inEntrata[i]);
		ricevutoAck[i]=false;
	}
	/*Creazione thread Mezzo*/
	for (t=0; t < threadMezzo; t++){
		p=malloc(sizeof(short int));
		if (p == NULL) {
			perror("malloc fallita\n");
			pthread_exit(NULL);
		}
		*p=5000+t;
	
		rc = pthread_create(&mezzo_threads[t], NULL, Mezzo, p);
		if (rc){
			printf("Errore: %d\n", rc);
			exit(1);
		}
		else{	
			#ifdef DEBUG_T
			printf(RED"Creato thread Mezzo\n"DEFAULTCOLOR);
			#endif
		}
	}
	SyncPoint(&sync_lock_mezzo,&sync_cond_mezzo,syncMain,&sync_count_mezzo);
	/*Creazione thread Sta*/
	for (t=0; t < threadSta; t++){
		p=malloc(sizeof(short int));
		if (p == NULL) {
			perror("malloc fallita\n");
			pthread_exit(NULL);
		}
		*p=5000+t;
		rc = pthread_create(&sta_threads[t], NULL, Sta, p);
		if (rc){
			printf("Errore: %d\n", rc);
			exit(-1);
		}else{
			#ifdef DEBUG_T
			printf(RED"Creato thread STA\n"DEFAULTCOLOR);
			#endif
		}
	}
	SyncPoint(&sync_lock_sta,&sync_cond_sta,syncMain,&sync_count_sta);
	milliSleep(2000);	
	/*Inizio test*/
	if(strcmp(customMsg,"")){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		printf("Messaggio personalizzato, prima applicazione lo invia all'ultima\n");
		milliSleep(2000);
		wt=5;
	}
	if(allTest || testBroadcast){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		printf("Inizio test, broadcast, non mi aspetto ne collisioni ne ack\n");
		milliSleep(2000);
	}
	
	if(allTest || testResponse){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		printf("Mando risposte, mi aspetto ack.\n");
		milliSleep(2000);
	}
	
	if(allTest || testCollision){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		printf("Mando contemporaneamente, mi aspetto collisioni\n");
		wt=threadSta*3;
		milliSleep(wt*1000);
	}
	
	if(allTest || testError){
		SyncPoint(&sync_lock_app,&sync_cond_app,syncApp,&sync_count_app);
		pthread_mutex_lock(&Msg_mutex);
		erroriCasuali=true;
		pthread_mutex_unlock(&Msg_mutex);
		printf("Test errori casuali\n");
		milliSleep(120000);
	}
	printf (RED"Tempo scaduto, chiudo applicazione!\n"DEFAULTCOLOR);
	pthread_mutex_destroy(&mezzo_aux);
	pthread_mutex_destroy(&app_mutex);
	pthread_mutex_destroy(&init_sta_mutex);
	pthread_mutex_destroy(&Msg_mutex);

	pthread_mutex_destroy(&sync_lock_app);
	pthread_cond_destroy(&sync_cond_app);
	
	pthread_mutex_destroy(&sync_lock_sta);
	pthread_cond_destroy(&sync_cond_sta);

	pthread_mutex_destroy(&sync_lock_mezzo_m);
	pthread_cond_destroy(&sync_cond_mezzo_m);	
	pthread_mutex_destroy(&sync_lock_mezzo);
	pthread_cond_destroy(&sync_cond_mezzo);
	
	return(0);
}

