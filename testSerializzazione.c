#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <fcntl.h>

typedef struct {
	char from[4];
	char to[4];
	char type[4];
	char text[100];
	char crc[2];
} sharedMex;
sharedMex mexToSend;
char serializeMex(char *output,sharedMex mex){
	sprintf(output,"%s%s%s%s%s",mex.from,mex.to,mex.type,mex.text,mex.crc);
	#ifdef DEBUGSERIAL
	printf("lunghezza serializemex output %d\n",strlen(output));
	#endif
}
void deSerializeMex(sharedMex *output, char *input){
	int i;
	sharedMex buf;
	char mess[2]="";
	strcpy(buf.text,mess);
	strcpy(buf.from,mess);
	strcpy(buf.to,mess);
	strcpy(buf.crc,mess);
	strcpy(buf.type,mess);
	#ifdef DEBUGSERIAL
	printf("lunghezza buf %d\n",strlen(input));
	#endif
	/*printf("test: %s %s %s %s \n",buf.from,buf.to,buf.text,buf.crc);
	printf("hola %d %c\n",strlen(input),input[1]);*/
	for (i=0;i<strlen(input);i++){
		if (i<=2){
			/*from*/
			sprintf(buf.from,"%s%c",buf.from,input[i]);
		}else{
			if (i<=5){
				/*to*/
				sprintf(buf.to,"%s%c",buf.to,input[i]);
			}else{
				if (i<=8){
					sprintf(buf.type,"%s%c",buf.type,input[i]);
				}else{
					if (i>=strlen(input)-1){
						/*crc*/
						sprintf(buf.crc,"%s%c",buf.crc,input[i]);
					}else{
						/*text*/
						sprintf(buf.text,"%s%c",buf.text,input[i]);
					}
				}
			}
		}
		#ifdef DEBUGSERIAL_LOOP
		printf("test: |%s| |%s| |%s| |%s| \n",buf.from,buf.to,buf.text,buf.crc);
		#endif
	}
	#ifdef DEBUGSERIAL
	printf(ORANGE"LET'S SEE: %s \n"DEFAULTCOLOR,buf.text);
	#endif
	*output = buf;
}

typedef struct{
	char data[2];
	char dataType[2];
	char toDS[2];
	char fromDS[2];
	char RTS[2];
	char CTS[2];
	char scan[2];
	char duration[2];
	char packetlen[5];
	char addr1[7];
	char addr2[7];
	char addr3[7];
	char addr4[7];
	char seqCtrl[5];
	char payload[2001];
	char crc[2];
} Frame;

int strToInt(char *in){
	int i;
	int v[4];
	for (i=0;i<strlen(in);i++){
		switch(in[i]){
			case '0':
				v[i]=0;
				break;
			case '1':
				v[i]=1;
				break;
			case '2':
				v[i]=2;
				break;
			case '3':
				v[i]=3;
				break;
			case '4':
				v[i]=4;
				break;
			case '5':
				v[i]=5;
				break;
			case '6':
				v[i]=6;
				break;
			case '7':
				v[i]=7;
				break;
			case '8':
				v[i]=8;
				break;
			case '9':
				v[i]=9;
				break;
			default:
				v[i]=0;
				break;
		}
	}
	return (v[0]*1000)+(v[1]*100)+(v[2]*10)+v[3];
}

int intToStr(char *out,int number){
	char buf[5];
	char aux[5];
	int n,i;
	strcpy(aux,"");
	strcpy(buf,"");
	sprintf(buf,"%d",number);
	n=4-strlen(buf);
	if (n>0){
		for (i=0;i<n;i++){
			sprintf(aux,"%s%c",aux,'0');
		}
	}
	strcat(aux,buf);
	strcpy(out,aux);
}


int coStr(int number){
	bool notfound=true;
	int i,j;
	char p,s;
	p='1';
	s='1';
	
	i=0;
	while ((i<=46)&&notfound){
		j=0;
		i++;
		while((j<=46)&&notfound){
			j++;
			if ((i*j)==number){
				notfound=false;
			}
		}
	}
	printf("%d %d\n",i,j);
	/*for (i=0;i<strlen(number);i++){
		switch(number[i]){
			case '1':
				v[i]=1;
				break;
			case '2':
				v[i]=2;
				break;
			case '3':
				v[i]=3;
				break;
			case '4':
				v[i]=4;
				break;
			case '5':
				v[i]=5;
				break;
			case '6':
				v[i]=6;
				break;
			case '7':
				v[i]=7;
				break;
			case '8':
				v[i]=8;
				break;
			case '9':
				v[i]=9;
				break;
			case '0':
				v[i]=10;
				break;
			case 'q':
				v[i]=11;
				break;
			case 'w':
				v[i]=12;
				break;
			case 'e':
				v[i]=13;
				break;
			case 'r':
				v[i]=14;
				break;
			case 't':
				v[i]=15;
				break;
			case 'y':
				v[i]=16;
				break;
			case 'u':
				v[i]=17;
				break;
			case 'i':
				v[i]=18;
				break;
			case 'o':
				v[i]=19;
				break;
			case 'p':
				v[i]=20;
				break;
			case 'a':
				v[i]=21;
				break;
			case 's':
				v[i]=22;
				break;
			case 'd':
				v[i]=23;
				break;
			case 'f':
				v[i]=23;
				break;
			case 'g':
				v[i]=24;
				break;
			case 'h':
				v[i]=25;
				break;
			case 'j':
				v[i]=26;
				break;
			case 'k':
				v[i]=27;
				break;
			case 'l':
				v[i]=28;
				break;
			case 'z':
				v[i]=29;
				break;
			case 'x':
				v[i]=30;
				break;
			case 'c':
				v[i]=31;
				break;
			case 'v':
				v[i]=32;
				break;
			case 'b':
				v[i]=33;
				break;
			case 'n':
				v[i]=34;
				break;
			case 'm':
				v[i]=35;
				break;
			case '-':
				v[i]=36;
				break;
			case '=':
				v[i]=37;
				break;
			case '[':
				v[i]=38;
				break;
			case ']':
				v[i]=39;
				break;
			case ';':
				v[i]=40;
				break;
			case ',':
				v[i]=41;
				break;
			case '.':
				v[i]=42;
				break;
			case '<':
				v[i]=43;
				break;
			case '>':
				v[i]=44;
				break;
			case '!':
				v[i]=45;
				break;
			case '@':
				v[i]=46;
				break;
			default:
				v[i]=1;
				break;
				
		}
	}*/
}

int decoStr(char *number){
	int i;
	int v[2];
	v[0]=1;
	v[1]=1;
	for (i=0;i<strlen(number);i++){
		switch(number[i]){
			case '1':
				v[i]=1;
				break;
			case '2':
				v[i]=2;
				break;
			case '3':
				v[i]=3;
				break;
			case '4':
				v[i]=4;
				break;
			case '5':
				v[i]=5;
				break;
			case '6':
				v[i]=6;
				break;
			case '7':
				v[i]=7;
				break;
			case '8':
				v[i]=8;
				break;
			case '9':
				v[i]=9;
				break;
			case '0':
				v[i]=10;
				break;
			case 'q':
				v[i]=11;
				break;
			case 'w':
				v[i]=12;
				break;
			case 'e':
				v[i]=13;
				break;
			case 'r':
				v[i]=14;
				break;
			case 't':
				v[i]=15;
				break;
			case 'y':
				v[i]=16;
				break;
			case 'u':
				v[i]=17;
				break;
			case 'i':
				v[i]=18;
				break;
			case 'o':
				v[i]=19;
				break;
			case 'p':
				v[i]=20;
				break;
			case 'a':
				v[i]=21;
				break;
			case 's':
				v[i]=22;
				break;
			case 'd':
				v[i]=23;
				break;
			case 'f':
				v[i]=23;
				break;
			case 'g':
				v[i]=24;
				break;
			case 'h':
				v[i]=25;
				break;
			case 'j':
				v[i]=26;
				break;
			case 'k':
				v[i]=27;
				break;
			case 'l':
				v[i]=28;
				break;
			case 'z':
				v[i]=29;
				break;
			case 'x':
				v[i]=30;
				break;
			case 'c':
				v[i]=31;
				break;
			case 'v':
				v[i]=32;
				break;
			case 'b':
				v[i]=33;
				break;
			case 'n':
				v[i]=34;
				break;
			case 'm':
				v[i]=35;
				break;
			case '-':
				v[i]=36;
				break;
			case '=':
				v[i]=37;
				break;
			case '[':
				v[i]=38;
				break;
			case ']':
				v[i]=39;
				break;
			case ';':
				v[i]=40;
				break;
			case ',':
				v[i]=41;
				break;
			case '.':
				v[i]=42;
				break;
			case '<':
				v[i]=43;
				break;
			case '>':
				v[i]=44;
				break;
			case '!':
				v[i]=45;
				break;
			case '@':
				v[i]=46;
				break;
			default:
				v[i]=1;
				break;
				
		}
	}
	return v[0]*v[1];
}


int frameSerialize(char *output,Frame frame){
	sprintf(output,
	"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"
	,frame.data,frame.dataType,frame.toDS,frame.fromDS,frame.RTS,frame.CTS,frame.scan,frame.duration,frame.packetlen,
	frame.addr1,frame.addr2,frame.addr3,frame.addr4,frame.seqCtrl,frame.payload,frame.crc);
}

int frameDeserialize(Frame *output, char *input){
	Frame aux;
	int i,j;
	int pck,ad1,ad2,ad3,ad4,sqc,pld;
	pck=8;
	ad1=pck+4;
	ad2=ad1+6;
	ad3=ad2+6;
	ad4=ad3+6;
	sqc=ad4+6;
	pld=sqc+4;
	printf("%d %d %d %d %d %d %d \n",pck,ad1,ad2,ad3,ad4,sqc,pld);
	sprintf(aux.data,"%c",input[0]);
	sprintf(aux.dataType,"%c",input[1]);
	sprintf(aux.toDS,"%c",input[2]);
	sprintf(aux.fromDS,"%c",input[3]);
	sprintf(aux.RTS,"%c",input[4]);
	sprintf(aux.CTS,"%c",input[5]);
	sprintf(aux.scan,"%c",input[6]);
	sprintf(aux.duration,"%c",input[7]);
	j=0;
	strcpy(aux.packetlen,"");
	for (i=pck;i<=ad1-1;i++){
		sprintf(aux.packetlen,"%s%c",aux.packetlen,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.addr1,"");
	for (i=ad1;i<=ad2-1;i++){
		sprintf(aux.addr1,"%s%c",aux.addr1,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.addr2,"");
	for (i=ad2;i<=ad3-1;i++){
		sprintf(aux.addr2,"%s%c",aux.addr2,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.addr3,"");
	for (i=ad3;i<=ad4-1;i++){
		sprintf(aux.addr3,"%s%c",aux.addr3,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.addr4,"");
	for (i=ad4;i<=sqc-1;i++){
		sprintf(aux.addr4,"%s%c",aux.addr4,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.seqCtrl,"");
	for (i=sqc;i<=pld-1;i++){
		sprintf(aux.seqCtrl,"%s%c",aux.seqCtrl,input[i]);
		j++;
	}
	j=0;
	strcpy(aux.payload,"");
	for (i=pld;i<=strlen(input)-2;i++){
		sprintf(aux.payload,"%s%c",aux.payload,input[i]);
		j++;
	}
	sprintf(aux.crc,"%c",input[strlen(input)-1]);
	*output=aux;
}


/*
int main(){
	sharedMex messaggio;
	int destinazione=110;
	int mittente=120;
	int crc=1;
	char mess[100]="sono una bella prova!";
	char mess2[100]="Cambio!";
	char buff[140];
	strcpy(messaggio.text,mess);
	sprintf(messaggio.from,"%d",mittente);
	sprintf(messaggio.to,"%d",destinazione);
	sprintf(messaggio.crc,"%d",crc);
	sprintf(messaggio.type,"%s","msg");
	printf("%s %s %s %s \n",messaggio.from,messaggio.to,messaggio.text,messaggio.crc);
	
	serializeMex(buff,messaggio);
	printf("%s \n",buff);
	printf("derp \n");
	strcpy(messaggio.text,mess2);
	sprintf(messaggio.from,"%d",200);
	sprintf(messaggio.to,"%d",300);
	sprintf(messaggio.crc,"%d",5);
	printf("%s %s %s %s \n",messaggio.from,messaggio.to,messaggio.text,messaggio.crc);
	deSerializeMex(&messaggio,buff);
	printf("%s %s %s %s \n",messaggio.from,messaggio.to,messaggio.text,messaggio.crc);
	/*
	sprintf(a,"%d",i);
	printf("%s %s\n",a,b);
	*//*
}
*/

int stampa(char *stringa){
	int i;
	for (i=0;i<strlen(stringa);i++){
		printf("%c:%d\n",stringa[i],i);
	}
}


int main(){
	Frame in,out;
	char a[9]="123456781";
	char inbuff[2050];
	strcpy(in.data,"1");
	strcpy(in.dataType,"2");
	strcpy(in.toDS,"3");
	strcpy(in.fromDS,"4");
	strcpy(in.RTS,"5");
	strcpy(in.CTS,"6");
	strcpy(in.scan,"7");
	strcpy(in.duration,"8");
	strcpy(in.packetlen,"2000");
	strcpy(in.addr1,">>>>>>");
	strcpy(in.addr2,"<<<<<<");
	strcpy(in.addr3,"||||||");
	strcpy(in.addr4,"::::::");
	strcpy(in.seqCtrl,"ciao");
	strcpy(in.payload,"HOLA GRINGO");
	strcpy(in.crc,"1");
	frameSerialize(inbuff,in);
	printf("serializzazione %s \n",inbuff);
	/*stampa(inbuff);*/
	frameDeserialize(&out,inbuff);
	printf(
	"risultato %s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s+%s\n"
	,out.data,out.dataType,out.toDS,out.fromDS,out.RTS,out.CTS,out.scan,out.duration,out.packetlen,
	out.addr1,out.addr2,out.addr3,out.addr4,out.seqCtrl,out.payload,out.crc);
	printf("traduzione: %d\n",strToInt("2000"));
	intToStr(inbuff,2000);
	printf("stampo %s \n",inbuff);
	
}
